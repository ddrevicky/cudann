Fully-Connected Neural Networks in CUDA
Dusan Drevicky

cudann/
  -----build
  -----include
  -----lib
  -----lic
  -----data
  -----src
  CMakelists.tx
  README.txt

To run the project:

1. Run cmake on the root directory and place the build files in the /build subdirectory.
2A. On Linux using make:
   a) cd to the build folder
   b) Run make.
2B. On Windows using Visual Studio:
   a) In Configuration Manager create an x64 solution platform and set it as active
   b) Right click the cudann project in Solution Explorer and select Build Dependencies -> Build Customizations. Select CUDA targets and apply.
   c) Exclude all .cu files from the project
   d) Add them back to the project again (They will now be compiled using NVCC. This is required because it decides which files to compile when they are added to a project.)
   e) Build solution.